
Moodle Theme: "Schola" 
===============================

![screenshot](pix/screenshot.jpg "Schola Screenshot")

---------

Schola is a modern Moodle Theme, built to be clean and very functional, It is built to suit Professional and Academic Learning environment,
Schola was also built for E-Learning Vendors, if you have more features in mind kindly reach out to us on support@retaniconsults.com

Schola is built on Boostrap 4 and Mustache templates.

Documentation
=============

WIP

Developed and maintained by
===========================

Retani Consults

Moodle profile: https://moodle.org/user/profile.php?id=968235

Linkedin: https://www.linkedin.com/company/53430316/admin/

Installation
------------

**First way**

- Clone this repository into the folder theme.
- Access the notification area in moodle and install

**Second way**

- Download this repository
- Extract the content
- Put the folder into the folder theme of your moodle
- Access the notification area in moodle and install