<?php
// Every file should have GPL and copyright in the header - we skip it in tutorials but you should not skip it for real.

// This line protects the file from being accessed by a URL directly.
defined('MOODLE_INTERNAL') || die();

// A description shown in the admin theme selector.
$string['choosereadme'] = 'Theme Schola is a child theme of Boost. It adds the ability to upload background photos.';
// The name of our plugin.
$string['pluginname'] = 'Schola';
// We need to include a lang string for each block region.
$string['login'] = 'Already have an account?';
$string['prev_section'] = 'Previous section';
$string['next_section'] = 'Next section';
$string['search_site'] = 'Search on site';
$string['search_forums'] = 'Search on course forums';
$string['platform_access'] = 'Access to the platform';
$string['readmore'] = 'Read More';
$string['stayintouch'] = 'Stay in touch';
$string['madewitmoodle'] = 'Proudly made with';
$string['madeby'] = 'Made with';
$string['by'] = 'by';
$string['discipline_progress'] = 'Topic\'s progress';
$string['access'] = 'Access';
$string['cachedef_admininfos'] = 'Site administrator dashboard infos';
$string['notcalculatedyet'] = 'Not calculated yet';
$string['calculatediskusagetask'] = 'Taks to calculate the disk usage';
$string['totalusers'] = 'Active / Suspended users';
$string['totalcourses'] = 'Total courses';
$string['onlineusers'] = 'Online users(last 5 minutes)';
$string['showhideblocks'] = 'Show/hide blocks'; 
$string['privacy:metadata'] = 'The Schola theme does not store any personal data about any user.';

// GENERAL LANGUAGE LOCALE
$string['configtitle']='Schola Theme Settings';
$string['theme_schola_general']='Schola General Settings';
$string['logo']='Logo';
$string['logodesc']='Upload Company / School Logo';
$string['frontpagesettings'] = 'Schola Frontpage Settings';
$string['sliderenabled'] = 'Enable Slide on the Frontpage';
$string['sliderenableddesc']='Toggles whether to Allow the Frontpage slider';
$string['slidercount']= 'Set the Number of Sliders on the Frontpage';
$string['slidercountdesc']='Setting the Number of slider for the Frontpage';
$string['sliderimage']='Upload an Image for the Slider';
$string['sliderimagedesc']='Uploads an Image to show on the frontpage slider';
$string['slidertitle'] = 'Enter the title for the Slider';
$string['slidertitledesc']='This description set for the slider';
$string['slidercaption']='Enter a caption for this Slider';
$string['slidercaptiondesc'] = 'The Caption will be rendered on the frontpage slider as the Slogan';
$string['welcometitle'] ='Welcome Title';
$string['welcometextdesc'] ='Welcome Title Description';
$string['welcometextdefault']='"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."';
$string['welcomeimage'] ='Welcome Image';
$string['welcomeimagedesc'] ='Welcome Image';
$string['slogantitle']='Slogan Title';
$string['slogantitledesc']='Enter the text to appear on Frontpage below the Welcome Section';
$string['slogantitledefault']='Slogan Title Default';
$string['slogantext']='Enter the text for the Heading Text';
$string['slogantextdesc']='This section is for the Heading Text Description';
$string['slogantextdefault']='SLogan default';
$string['courseblock1']='Block 1';
$string['courseblock1desc']='Edit Block 1 HTML Content';
$string['courseblock2']='Block 2';
$string['courseblock2desc']='Edit Block 2 HTML Content';
$string['courseblock3']='Block 3';
$string['courseblock3desc']='Edit Block 3 HTML Content';
$string['courseblock4']='Block 4';
$string['courseblock4desc']='Edit Block 4 HTML Content';
$string['tutorenabled']='Display Tutors ?';
$string['tutorenableddesc']='You have control over showing tutors on the Frontpage';
$string['brochure_text_title']='Enter Title for Brochure';
$string['brochure_text_desc']='Enter the Description for the Brochure Title';
$string['brochure_button_text']='Brochure Button Text';
$string['brochure_button_text_desc']='This allows you to set a text to the button';
$string['brochure_image_title']='Brochure Image Title';
$string['brochure_image_desc']='Upload image to show in the brochure section';
$string['footer_text']='Footer HTML Content';



