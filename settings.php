<?php
// This file is part of Ranking block for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme schola block settings file
 *
 * @package    theme_schola
 * @copyright  2020 Retani Consults http://retaniconsults.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// This line protects the file from being accessed by a URL directly.
defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    // Boost provides a nice setting page which splits settings onto separate tabs. We want to use it here.
    $settings = new theme_boost_admin_settingspage_tabs('themesettingschola', get_string('configtitle', 'theme_schola'));

    /*
    * ----------------------
    * General settings tab
    * ----------------------
    */
    $page = new admin_settingpage('theme_schola_general', get_string('generalsettings', 'theme_schola'));

    // Logo file setting.
    $name = 'theme_schola/logo';
    $title = get_string('logo', 'theme_schola');
    $description = get_string('logodesc', 'theme_schola');
    $opts = array('accepted_types' => array('.png', '.jpg', '.gif', '.webp', '.tiff', '.svg'), 'maxfiles' => 1);
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo', 0, $opts);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Add Frontpage Settings
    $page = new admin_settingpage('theme_schola_frontpage', get_string('frontpagesettings', 'theme_schola'));

    // Enable or disable Slideshow settings.
    $name = 'theme_schola/sliderenabled';
    $title = get_string('sliderenabled', 'theme_schola');
    $description = get_string('sliderenableddesc', 'theme_schola');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $page->add($setting);

    // Set the Number of Sliders
    $name = 'theme_schola/slidercount';
    $title = get_string('slidercount', 'theme_schola');
    $description = get_string('slidercountdesc', 'theme_schola');
    $default = 1;
    $options = array();
    for ($slideridx = 0; $slideridx < 13; $slideridx++) {
        $options[$slideridx] = $slideridx;
    }
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // If we don't have an slide yet, default to the preset.
    $slidercount = get_config('theme_schola', 'slidercount');

    if (!$slidercount) {
        $slidercount = 1;
    }

    for ($sliderindex = 1; $sliderindex <= $slidercount; $sliderindex++) {
        $fileid = 'sliderimage' . $sliderindex;
        $name = 'theme_schola/sliderimage' . $sliderindex;
        $title = get_string('sliderimage', 'theme_schola');
        $description = get_string('sliderimagedesc', 'theme_schola');
        $opts = array('accepted_types' => array('.png', '.jpg', '.gif', '.webp', '.tiff', '.svg'), 'maxfiles' => 1);
        $setting = new admin_setting_configstoredfile($name, $title, $description, $fileid, 0, $opts);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $page->add($setting);

        $name = 'theme_schola/slidertitle' . $sliderindex;
        $title = get_string('slidertitle', 'theme_schola');
        $description = get_string('slidertitledesc', 'theme_schola');
        $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_TEXT);
        $page->add($setting);

        $name = 'theme_schola/slidercap' . $sliderindex;
        $title = get_string('slidercaption', 'theme_schola');
        $description = get_string('slidercaptiondesc', 'theme_schola');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $page->add($setting);
    }

    // Welcome Title
    $name = 'theme_schola/welcometitle';
    $title = get_string('welcometitle', 'theme_schola');
    $description = get_string('welcometitledesc', 'theme_schola');
    $welcomeDefault = get_string('welcometitledefault', 'theme_schola');
    $settings = new admin_setting_configtext($name, $title, $description, $welcomeDefault, PARAM_TEXT);
    $page->add($settings);

    // Welcome Content
    $name = 'theme_schola/welcometext';
    $title = get_string('welcometext', 'theme_schola');
    $description = get_string('welcometextdesc', 'theme_schola');
    $welcomeDefault = get_string('welcometextdefault', 'theme_schola');
    $settings = new admin_setting_confightmleditor($name, $title, $description, $welcomeDefault, PARAM_TEXT);
    $page->add($settings);

    // Welcome Image
    $name = 'theme_schola/welcomeimage';
    $fileId = 'welcomeimage_01';
    $title = get_string('welcomeimage', 'theme_schola');
    $description = get_string('welcomeimagedesc', 'theme_schola');
    $welcomeDefault = get_string('welcomeimagedefault', 'theme_schola');
    $opts = array('accepted_types' => array('.png', '.jpg', '.gif', '.webp', '.tiff', '.svg'), 'maxfiles' => 1);
    $settings = new admin_setting_configstoredfile($name, $title, $description, $fileId, 0, $opts);
    $page->add($settings);

    // Slogan Title
    $name = 'theme_schola/slogantitle';
    $title = get_string('slogantitle', 'theme_schola');
    $description = get_string('slogantitledesc', 'theme_schola');
    $default = get_string('slogantitledefault', 'theme_schola');
    $settings = new admin_setting_configtext($name, $title, $description, $default, PARAM_TEXT);
    $page->add($settings);

    // Slogan Content
    $name = 'theme_schola/slogantext';
    $title = get_string('slogantext', 'theme_schola');
    $description = get_string('slogantextdesc', 'theme_schola');
    $default = get_string('slogantextdefault', 'theme_schola');
    $settings = new admin_setting_confightmleditor($name, $title, $description, $default);
    $page->add($settings);

    // Course Information Block 1
    $name = 'theme_schola/courseblock1';
    $title = get_string('courseblock1', 'theme_schola');
    $description = get_string('courseblock1desc', 'theme_schola');
    $default = '<div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
          <strong>5000+</strong>
          <span>Learners</span>
        </div>';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Course Information Block 2
    $name = 'theme_schola/courseblock2';
    $title = get_string('courseblock2', 'theme_schola');
    $description = get_string('courseblock2desc', 'theme_schola');
    $default = '<div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
          <strong>1000+</strong>
          <span>Courses</span>
        </div>';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Course Information Block 3
    $name = 'theme_schola/courseblock3';
    $title = get_string('courseblock3', 'theme_schola');
    $description = get_string('courseblock3desc', 'theme_schola');
    $default = '<div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
          <strong>2000+</strong>
          <span>Facilitators</span>
        </div>';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Course Information Block 4
    $name = 'theme_schola/courseblock4';
    $title = get_string('courseblock4', 'theme_schola');
    $description = get_string('courseblock4desc', 'theme_schola');
    $default = '<div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
          <strong>5</strong>
          <span>Countries</span>
        </div>';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Tutors: Enable Tutors for the Frontpage

    $name = 'theme_schola/tutorsnabled';
    $title = get_string('tutorenabled', 'theme_schola');
    $description = get_string('tutorenableddesc', 'theme_schola');
    $default = 'Enable Tutor Components';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Brochure Information Text
    $name = 'theme_schola/brochure_text';
    $title = get_string('brochure_text_title', 'theme_schola');
    $description = get_string('brochure_text_desc', 'theme_schola');
    $default = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_RAW);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Brochure Caption Button Text
    $name = 'theme_schola/brochure_button_text';
    $title = get_string('brochure_button_text', 'theme_schola');
    $description = get_string('brochure_button_text_desc', 'theme_schola');
    $default = 'Download Brochure';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_RAW);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Brochure Information Image
    $name = 'theme_schola/brochure_image';
    $title = get_string('brochure_image_title', 'theme_schola');
    $fileId = $name . "01";
    $description = get_string('brochure_image_desc', 'theme_schola');
    $default = 'Enable Tutor Components';
    $opts = array('accepted_types' => array('.png', '.jpg', '.gif', '.webp', '.tiff', '.svg'), 'maxfiles' => 1);
    $setting = new admin_setting_configstoredfile($name, $title, $description, $fileId, 0, $opts);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Footer Left Block
    $name = 'theme_schola/footer_text';
    $title = get_string('footer_text', 'theme_schola');
    $fileId = $name . "01";
    $description = get_string('footer_text_desc', 'theme_schola');
    $default = '<p class="mt-4">
                Lorem ipsum dolor sit amet, <br> consetetur sadipscing elitr, sed diam <br> nonumy eirmod tempor invidunt ut
                <br> labore et dolore magna aliquyam erat <br>
            </p>
            <p>
                200 N Adeola Street <br>
                Suite 500 Medina Estate <br>
                Rigamortis Avenue <br>
            </p>';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
}